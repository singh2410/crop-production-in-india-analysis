#!/usr/bin/env python
# coding: utf-8

# # Crop Production in India Analysis
# #By- Aarush Kumar
# #Dated: June 29,2021

# In[4]:


import numpy as np
import pandas as pd
from IPython.display import Image
Image(url='https://www.theindiaforum.in/sites/default/files/field/image/2021/05/28/ramkumar-radhakrishnan-wikimedia-1622193304.jpeg')


# In[5]:


import seaborn as sns
import matplotlib.pyplot as plt


# In[6]:


df=pd.read_csv("/home/aarush100616/Downloads/Projects/Crop Production in India Analysis/crop_production.csv")


# In[7]:


df


# In[8]:


df.head()


# In[9]:


df.info()


# In[10]:


df.isnull().sum()


# In[11]:


df.isnull().any()


# In[12]:


df.shape


# In[13]:


df.size


# ### Predicting production in india on the basis of this dataset

# In[14]:


df.dropna(subset=["Production"],axis=0,inplace=True)


# In[15]:


df.info()


# In[16]:


df.isnull().sum()


# In[17]:


df.isnull().any()


# ## Exploratory Data Analysis

# In[18]:


df.columns


# In[19]:


sns.boxplot(x=df["State_Name"],y=df["Production"])


# In[20]:


sns.boxplot(x=df["District_Name"],y=df["Production"])


# In[21]:


sns.boxplot(x=df["Crop_Year"],y=df["Production"])


# In[22]:


sns.boxplot(x=df["Season"],y=df["Production"])


# In[28]:


sns.boxplot(x=df["Crop"],y=df["Production"])


# In[24]:


plt.tick_params(labelsize=10)
sns.heatmap(df.corr(),annot=True)


# In[25]:


df["State_Name"].unique()


# In[26]:


plt.tick_params(labelsize=10)
df.groupby("Crop_Year")["Production"].agg("sum").plot.bar()


# In[27]:


plt.figure(figsize=(25,8))
plt.tick_params(labelsize=10)
df.groupby("Crop")["Production"].agg("count").plot.bar()
plt.show()


# In[29]:


cc=df['Crop']
def cat_crop(cc):
    for i in ['Rice','Maize','Wheat','Barley','Varagu','Other Cereals & Millets','Ragi','Small millets','Bajra','Jowar']:
        if cc==i:
            return 'Cereal'
    for i in ['Moong','Urad','Arhar/Tur','Peas & beans','Masoor',
              'Other Kharif pulses','other misc. pulses','Ricebean (nagadal)',
              'Rajmash Kholar','Lentil','Samai','Blackgram','Korra','Cowpea(Lobia)',
              'Other  Rabi pulses','Other Kharif pulses','Peas & beans (Pulses)']:
        if cc==i:
            return 'Pulses'
    for i in ['Peach','Apple','Litchi','Pear','Plums','Ber','Sapota','Lemon','Pome Granet',
               'Other Citrus Fruit','Water Melon','Jack Fruit','Grapes','Pineapple','Orange',
               'Pome Fruit','Citrus Fruit','Other Fresh Fruits','Mango','Papaya','Coconut','Banana']:
        if cc==i:
            return 'Fruits'
    for i in ['Bean','Lab-Lab','Moth','Guar seed','Tapioca','Soyabean','Horse-gram','Gram']:
        if cc==i:
            return 'Beans'
    for i in ['Turnip','Peas','Beet Root','Carrot','Yam','Ribed Guard','Ash Gourd ','Pump Kin','Redish','Snak Guard','Bottle Gourd',
              'Bitter Gourd','Cucumber','Drum Stick','Cauliflower','Beans & Mutter(Vegetable)','Cabbage',
              'Bhindi','Tomato','Brinjal','Khesari','Sweet potato','Potato','Onion']:
        if cc==i:
            return 'Vegetables'
    for i in ['Perilla','Colocosia','Ginger','Cardamom','Black pepper','Dry ginger','Garlic','Coriander','Turmeric','Dry chillies']:
        if cc==i:
            return 'Species'
    for i in ['Jobster','Cond-spcs other']:
        if cc==i:
            return 'Other'
    for i in ['other fibres','Kapas','Jute & mesta','Jute','Mesta','Cotton(lint)']:
        if cc==i:
            return 'fibres'
    for i in ['Arcanut (Processed)','Atcanut (Raw)','Cashewnut Processed','Cashewnut Raw','Cashewnut','Arecanut','Groundnut']:
        if cc==i:
            return 'Nuts'
    for i in ['Rubber']:
        if cc==i:
            return 'Natural Polymer'
    for i in ['Coffee']:
        if cc== i:
            return 'Coffee'
    for i in ['Tea']:
        if cc==i:
            return 'Tea'
    for i in ['Total foodgrain']:
        if cc==i:
            return 'Total foodgrain'
    for i in ['Pulses total']:
        if cc==i:
            return 'Pulses total'
    for i in ['Oilseeds total']:
        if cc==i:
            return 'Oilseeds total'
    for i in ['Paddy']:
        if cc==i:
            return 'Paddy'
    for i in ['other oilseeds','Safflower','Niger seed','Castor seed','Linseed','Sunflower','Rapeseed &Mustard','Sesamum']:
        if cc==i:
            return 'Oilseeds'
    for i in ['Sannhamp']:
        if cc==i:
            return 'Fertile Plant'
    for i in ['Tobacco']:
        if cc==i:
            return 'Commercial'
    for i in ['Sugarcane']:
        if cc==i:
            return 'Sugarcane'


# In[30]:


df['cat_crop']=df['Crop'].apply(cat_crop)


# In[31]:


df.head()


# In[32]:


df["cat_crop"].value_counts()


# In[33]:


plt.figure(figsize=(20,8))
sns.scatterplot(data=df,x="State_Name",y="cat_crop",hue="Season")
plt.xticks(rotation=90)
plt.show()


# In[34]:


df1=df["cat_crop"].value_counts()
df1.plot(radius=3,kind="pie",autopct="%1.1f",pctdistance=0.6)
plt.tick_params(labelsize=10)


# In[35]:


df2 = pd.crosstab(df['State_Name'], df['cat_crop'])
df2.plot(kind='bar', stacked=True, figsize = (16,8))

